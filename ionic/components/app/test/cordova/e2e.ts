
it('should navigate to page 2', function() {
  element(by.css('.e2eCordovaPage2')).click();
});

it('should open modal', function() {
  element(by.css('.e2eCordovaOpenModal')).click();
});
