System.config({
  map: {
    'angular2': '/base/angular2',
    'ionic-angular': '/base/ionic'
  },
  packages: {
    'ionic-angular': {
      main: 'index'
    }
  }
});